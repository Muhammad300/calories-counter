package com.anroid.ejazz.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class ConsumedCaloriesModel implements Parcelable {
    protected ConsumedCaloriesModel(Parcel in) {
        itemName = in.readString();
        calories = in.readDouble();
        time = in.readString();
    }

    public ConsumedCaloriesModel() {

    }

    public static final Creator<ConsumedCaloriesModel> CREATOR = new Creator<ConsumedCaloriesModel>() {
        @Override
        public ConsumedCaloriesModel createFromParcel(Parcel in) {
            return new ConsumedCaloriesModel(in);
        }

        @Override
        public ConsumedCaloriesModel[] newArray(int size) {
            return new ConsumedCaloriesModel[size];
        }
    };

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getCalories() {
        return calories;
    }

    public void setCalories(double calories) {
        this.calories = calories;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    private String itemName;
    private double calories;
    private String time;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(itemName);
        dest.writeDouble(calories);
        dest.writeString(time);
    }
}
