package com.anroid.ejazz.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class CaloriesModel implements Parcelable{

    @SerializedName("total_hits")
    @Expose
    private Integer totalHits;
    @SerializedName("max_score")
    @Expose
    private Double maxScore;
    @SerializedName("hits")
    @Expose
    private List<Hit> hits = null;

    protected CaloriesModel(Parcel in) {
    }

    public static final Creator<CaloriesModel> CREATOR = new Creator<CaloriesModel>() {
        @Override
        public CaloriesModel createFromParcel(Parcel in) {
            return new CaloriesModel(in);
        }

        @Override
        public CaloriesModel[] newArray(int size) {
            return new CaloriesModel[size];
        }
    };

    public Integer getTotalHits() {
        return totalHits;
    }

    public void setTotalHits(Integer totalHits) {
        this.totalHits = totalHits;
    }

    public Double getMaxScore() {
        return maxScore;
    }

    public void setMaxScore(Double maxScore) {
        this.maxScore = maxScore;
    }

    public List<Hit> getHits() {
        return hits;
    }

    public void setHits(List<Hit> hits) {
        this.hits = hits;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }


    public static class Hit implements Parcelable{

            @SerializedName("_index")
            @Expose
            private String index;
            @SerializedName("_type")
            @Expose
            private String type;
            @SerializedName("_id")
            @Expose
            private String id;
            @SerializedName("_score")
            @Expose
            private Double score;
            @SerializedName("fields")
            @Expose
            private Fields fields;

        protected Hit(Parcel in) {
            index = in.readString();
            type = in.readString();
            id = in.readString();
        }

        public static final Creator<Hit> CREATOR = new Creator<Hit>() {
            @Override
            public Hit createFromParcel(Parcel in) {
                return new Hit(in);
            }

            @Override
            public Hit[] newArray(int size) {
                return new Hit[size];
            }
        };

        public String getIndex() {
                return index;
            }

            public void setIndex(String index) {
                this.index = index;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public Double getScore() {
                return score;
            }

            public void setScore(Double score) {
                this.score = score;
            }

            public Fields getFields() {
                return fields;
            }

            public void setFields(Fields fields) {
                this.fields = fields;
            }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(index);
            dest.writeString(type);
            dest.writeString(id);
        }

        public static class Fields implements  Parcelable {

                @SerializedName("item_id")
                @Expose
                private String itemId;
                @SerializedName("item_name")
                @Expose
                private String itemName;
                @SerializedName("brand_id")
                @Expose
                private String brandId;
                @SerializedName("brand_name")
                @Expose
                private String brandName;
                @SerializedName("nf_serving_size_qty")
                @Expose
                private Integer nfServingSizeQty;
                @SerializedName("nf_serving_size_unit")
                @Expose
                private String nfServingSizeUnit;

            protected Fields(Parcel in) {
                itemId = in.readString();
                itemName = in.readString();
                brandId = in.readString();
                brandName = in.readString();
                nfServingSizeUnit = in.readString();
            }

            public static final Creator<Fields> CREATOR = new Creator<Fields>() {
                @Override
                public Fields createFromParcel(Parcel in) {
                    return new Fields(in);
                }

                @Override
                public Fields[] newArray(int size) {
                    return new Fields[size];
                }
            };

            public double getNf_calories() {
                    return nf_calories;
                }

                public void setNf_calories(double nf_calories) {
                    this.nf_calories = nf_calories;
                }

                @SerializedName("nf_calories")
                @Expose
                private double nf_calories;



                public String getItemId() {
                    return itemId;
                }

                public void setItemId(String itemId) {
                    this.itemId = itemId;
                }

                public String getItemName() {
                    return itemName;
                }

                public void setItemName(String itemName) {
                    this.itemName = itemName;
                }

                public String getBrandId() {
                    return brandId;
                }

                public void setBrandId(String brandId) {
                    this.brandId = brandId;
                }

                public String getBrandName() {
                    return brandName;
                }

                public void setBrandName(String brandName) {
                    this.brandName = brandName;
                }

                public Integer getNfServingSizeQty() {
                    return nfServingSizeQty;
                }

                public void setNfServingSizeQty(Integer nfServingSizeQty) {
                    this.nfServingSizeQty = nfServingSizeQty;
                }

                public String getNfServingSizeUnit() {
                    return nfServingSizeUnit;
                }

                public void setNfServingSizeUnit(String nfServingSizeUnit) {
                    this.nfServingSizeUnit = nfServingSizeUnit;
                }

            @Override
            public int describeContents() {
                return 0;
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeString(itemId);
                dest.writeString(itemName);
                dest.writeString(brandId);
                dest.writeString(brandName);
                dest.writeString(nfServingSizeUnit);
            }
        }
    }
}




