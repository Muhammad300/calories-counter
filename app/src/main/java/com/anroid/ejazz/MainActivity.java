package com.anroid.ejazz;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.anroid.ejazz.fragment.ConsumedCalories;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.mainContainer, new ConsumedCalories()).commit();

    }
}
