package com.anroid.ejazz.storage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;

import com.anroid.ejazz.R;
import com.anroid.ejazz.fragment.ConsumedCalories;
import com.anroid.ejazz.fragment.SearchCalories;
import com.anroid.ejazz.model.ConsumedCaloriesModel;
import com.anroid.ejazz.utils.Constants;

import java.util.ArrayList;

import static android.provider.BaseColumns._ID;
import static com.anroid.ejazz.storage.DatabaseContract.DataEntry.COLUMN_CALORIES;
import static com.anroid.ejazz.storage.DatabaseContract.DataEntry.COLUMN_ITEM_NAME;
import static com.anroid.ejazz.storage.DatabaseContract.DataEntry.COLUMN_TIME;
import static com.anroid.ejazz.storage.DatabaseContract.DataEntry.TABLE_NAME;

/**
 * Created by Muhammad on 9/27/2016.
 */

public class Database extends SQLiteOpenHelper {
    private ArrayList<ConsumedCaloriesModel> caloriesModels;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Ejaaz.db";
    private static final String TEXT_TYPE = " TEXT";
    private static final String REAL_TYPE = " REAL";
    private ConsumedCalories consumedCalories;
    private SearchCalories searchCalories;
    private Context context;
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    _ID + " INTEGER PRIMARY KEY," +
                    COLUMN_ITEM_NAME + TEXT_TYPE +
                    COMMA_SEP +
                    COLUMN_CALORIES + REAL_TYPE +
                    COMMA_SEP +
                    COLUMN_TIME + TEXT_TYPE +
                    " )";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + TABLE_NAME;


    public Database(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        caloriesModels = new ArrayList<>();
        this.context = context;
    }

    public Database(Context context, ConsumedCalories consumedCalories) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        caloriesModels = new ArrayList<>();
        this.consumedCalories = consumedCalories;
        this.context = context;
    }

    public Database(Context context, SearchCalories searchCalories) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        caloriesModels = new ArrayList<>();
        this.searchCalories = searchCalories;
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        Log.e("database", SQL_CREATE_ENTRIES);
        sqLiteDatabase.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);

    }

    Cursor cursor;

    private int getCursorSize() {
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sortOrder =
                    _ID + " DESC";

            cursor = db.query(
                    TABLE_NAME,                     // The table to query
                    null,                               // The columns to return
                    null,                                // The columns for the WHERE clause
                    null,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );
            cursor.moveToFirst();

            return cursor.getCount();
        } catch (Exception e) {
            e.printStackTrace();
            return cursor.getCount();
        }
    }


    public ArrayList<ConsumedCaloriesModel> readCaloriesData(Bundle bundle) {
        try {
            caloriesModels.clear();
            SQLiteDatabase db = this.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
            String[] projection = {
                    _ID,
                    COLUMN_ITEM_NAME,
                    COLUMN_CALORIES,
                    COLUMN_TIME
            };

            String itemName = bundle.getString(Constants.ItemName, "");
            String selection = COLUMN_ITEM_NAME + " LIKE ?";
            String[] selectionArgs = {"%" + itemName + "%"};

// How you want the results sorted in the resulting Cursor
            String sortOrder =
                    _ID + " DESC";

            Cursor c = db.query(
                    TABLE_NAME,                     // The table to query
                    projection,                               // The columns to return
                    selection,                                // The columns for the WHERE clause
                    selectionArgs,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );
            c.moveToFirst();
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    addData(c);
                    c.moveToNext();
                }
            }
            c.close();
            db.close();
            if (consumedCalories != null)
                consumedCalories.onCaloriesLoaded(caloriesModels);
            return caloriesModels;
        } catch (Exception e) {
            e.printStackTrace();
            if (consumedCalories != null)
                consumedCalories.onCaloriesLoadError(context.getString(R.string.error));
            return caloriesModels;
        }
    }

    public long getTotalCalories() {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cur = db.rawQuery("SELECT SUM("+COLUMN_CALORIES +")FROM "+TABLE_NAME, null);
        if (cur.moveToFirst()) {
            long total = cur.getLong(0);
            cur.close();
            return total;
        }
        return 0;
    }

    public ArrayList<ConsumedCaloriesModel> readCaloriesData() {
        try {
            caloriesModels.clear();
            SQLiteDatabase db = this.getReadableDatabase();

// Define a projection that specifies which columns from the database
// you will actually use after this query.
            String[] projection = {
                    _ID,
                    COLUMN_ITEM_NAME,
                    COLUMN_CALORIES,
                    COLUMN_TIME
            };

// Filter results WHERE "title" = 'key word'
            String selection = _ID + " = ?";
            String[] selectionArgs = {"My Title"};

// How you want the results sorted in the resulting Cursor
            String sortOrder =
                    _ID + " DESC";

            Cursor c = db.query(
                    TABLE_NAME,                     // The table to query
                    projection,                               // The columns to return
                    null,                                // The columns for the WHERE clause
                    null,                            // The values for the WHERE clause
                    null,                                     // don't group the rows
                    null,                                     // don't filter by row groups
                    sortOrder                                 // The sort order
            );
            c.moveToFirst();
            String[] str = new String[getCursorSize()];
            if (c.moveToFirst()) {
                while (!c.isAfterLast()) {
                    addData(c);
                    c.moveToNext();
                }
            }
            c.close();
            db.close();
            if (consumedCalories != null)
                consumedCalories.onCaloriesLoaded(caloriesModels);
            return caloriesModels;
        } catch (Exception e) {
            e.printStackTrace();
            if (consumedCalories != null)
                consumedCalories.onCaloriesLoadError(context.getString(R.string.error));
            return caloriesModels;
        }
    }


    public void insertCaloriesData(Bundle bundle) {

        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(DatabaseContract.DataEntry.COLUMN_ITEM_NAME, bundle.getString(Constants.ItemName, ""));
            values.put(COLUMN_CALORIES, bundle.getDouble(Constants.Calories, 0.0));
            values.put(COLUMN_TIME, bundle.getString(Constants.Time, ""));
            db.insert(TABLE_NAME, null, values);
            db.close();
            if (searchCalories != null)
                searchCalories.onCaloriesAdded(context.getString(R.string.calories_per_day));
        } catch (Exception e) {
            e.printStackTrace();
            if (searchCalories != null)
                searchCalories.onCaloriesError(context.getString(R.string.error));
        }
    }

    public void deleteConsumedCalories() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, null, null);

    }

    private void addData(Cursor c) {
        ConsumedCaloriesModel consumedCaloriesModel = new ConsumedCaloriesModel();
        consumedCaloriesModel.setItemName(c.getString(c.getColumnIndexOrThrow(COLUMN_ITEM_NAME)));
        consumedCaloriesModel.setCalories(c.getDouble(c.getColumnIndexOrThrow(COLUMN_CALORIES)));
        consumedCaloriesModel.setTime(c.getString(c.getColumnIndexOrThrow(COLUMN_TIME)));
        caloriesModels.add(consumedCaloriesModel);
    }
}
