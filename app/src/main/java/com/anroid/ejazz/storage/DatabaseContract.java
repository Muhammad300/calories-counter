package com.anroid.ejazz.storage;

import android.provider.BaseColumns;

/**
 * Created by Muhammad on 9/27/2016.
 */

public final class DatabaseContract {
        // To prevent someone from accidentally instantiating the contract class,
        // make the constructor private.
        private DatabaseContract() {}

        /* Inner class that defines the table contents */
        public static class DataEntry implements BaseColumns {
            public static final String TABLE_NAME = "calories";
            public static final String COLUMN_ITEM_NAME = "itemName";
            public static final String COLUMN_CALORIES = "calories";
            public static final String COLUMN_TIME = "time";


        }
}
