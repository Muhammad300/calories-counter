package com.anroid.ejazz.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;

import com.anroid.ejazz.storage.Database;
import com.anroid.ejazz.utils.Constants;

import java.util.Calendar;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class CleanerService extends Service {
    Database database;
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        mServiceHandler.sendMessage(msg);

        database= new Database(this);


        Calendar calendar = Calendar.getInstance();

        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Intent cleanIntent= new Intent(this, CleanerBroadCast.class);
        intent.setAction(Constants.CLEAN_ACTION);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                cleanIntent ,PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager am = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        am.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),
                AlarmManager.INTERVAL_DAY, pendingIntent);
        return START_STICKY;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread handlerThread = new HandlerThread("message");
        handlerThread.start();
        mServiceLooper = handlerThread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }


    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            try {

            } catch (Exception e) {
                Thread.currentThread().interrupt();
            }
        }
    }
}
