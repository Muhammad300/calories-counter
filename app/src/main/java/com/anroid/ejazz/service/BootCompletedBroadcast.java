package com.anroid.ejazz.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class BootCompletedBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            Intent startServiceIntent = new Intent(context, CleanerService.class);
            context.startService(startServiceIntent);
        }
    }
}