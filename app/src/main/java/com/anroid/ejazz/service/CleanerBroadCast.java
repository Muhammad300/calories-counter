package com.anroid.ejazz.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.anroid.ejazz.storage.Database;
import com.anroid.ejazz.utils.Constants;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class CleanerBroadCast extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equalsIgnoreCase(Constants.CLEAN_ACTION)){
            Database database=new Database(context);
            database.deleteConsumedCalories();
        }


    }
}
