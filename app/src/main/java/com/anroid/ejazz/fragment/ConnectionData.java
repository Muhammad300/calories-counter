package com.anroid.ejazz.fragment;

import java.util.Map;

/**
 * Created by Muhammad on 11/3/2016.
 */

public abstract  class ConnectionData{

    public abstract void fetch(String keyword);
    public abstract  String getURL(String keyword);
    public  abstract Map<String,String> getParams();
    public abstract void cancel();

}
