package com.anroid.ejazz.fragment;

import android.support.v4.app.Fragment;

/**
 * Created by Muhammad on 11/3/2016.
 */

public abstract class CaloriesData<T> extends Fragment {
    public  abstract void onSuccess(T data);
    public  abstract void onError(String error);



}
