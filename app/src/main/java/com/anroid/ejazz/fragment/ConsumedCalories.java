package com.anroid.ejazz.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.anroid.ejazz.R;
import com.anroid.ejazz.adapter.ConsumedCaloriesAdapter;
import com.anroid.ejazz.listeners.CaloriesLoaded;
import com.anroid.ejazz.model.ConsumedCaloriesModel;
import com.anroid.ejazz.storage.Database;
import com.anroid.ejazz.utils.Constants;
import com.anroid.ejazz.utils.Toaster;
import com.github.lzyzsd.circleprogress.ArcProgress;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class ConsumedCalories extends CaloriesData implements View.OnClickListener, TextWatcher, SwipeRefreshLayout.OnRefreshListener, CaloriesLoaded {
    @BindView(R.id.caloriesRecycler)
    RecyclerView caloriesRecycler;
    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.arc_progress)
    ArcProgress arcProgress;
    @BindView(R.id.searchItems)
    EditText search;
    @BindView(R.id.caloriesCount)
    TextView caloriesCount;

    @BindView(R.id.add_item)
    Button addItem;

    private Database database;
    private Bundle bundle;
    private Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        database = new Database(getActivity(), this);

        bundle = new Bundle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_consumed_calories, container, false);
        unbinder = ButterKnife.bind(this, view);
        caloriesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        caloriesRecycler.setHasFixedSize(true);
        search.addTextChangedListener(this);
        SlideInUpAnimator animator = new SlideInUpAnimator(new OvershootInterpolator(1f));
        caloriesRecycler.setItemAnimator(animator);
        addItem.setOnClickListener(this);
        swipeRefresh.setOnRefreshListener(this);
        return view;
    }

    ConsumedCaloriesAdapter consumedCaloriesAdapter;

    @Override
    public void onStart() {
        super.onStart();
        database.readCaloriesData();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onSuccess(Object data) {


    }

    @Override
    public void onError(String error) {

    }

    @Override
    public void onClick(View v) {
        if (v.equals(addItem)) {
            getActivity().getSupportFragmentManager().beginTransaction().addToBackStack("").replace(R.id.mainContainer, new SearchCalories()).commit();

        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        swipeRefresh.setRefreshing(true);
        bundle.putString(Constants.ItemName, s.toString());
        database.readCaloriesData(bundle);
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onRefresh() {
        if (TextUtils.isEmpty(search.getText().toString())) {
            swipeRefresh.setRefreshing(true);
            database.readCaloriesData();
        } else {
            swipeRefresh.setRefreshing(true);
            bundle.putString(Constants.ItemName, search.getText().toString());
            database.readCaloriesData(bundle);
        }
    }


    @Override
    public void onCaloriesLoaded(Object data) {
        try {
            ArrayList<ConsumedCaloriesModel> consumedCaloriesModel = (ArrayList<ConsumedCaloriesModel>) data;


            consumedCaloriesAdapter = new ConsumedCaloriesAdapter(getActivity(), consumedCaloriesModel, getActivity().getSupportFragmentManager());
            consumedCaloriesAdapter.notifyDataSetChanged();
            caloriesRecycler.setAdapter(consumedCaloriesAdapter);
            consumedCaloriesAdapter.remove(0);
            consumedCaloriesAdapter.add(0);
            double consumedCalories = database.getTotalCalories();
            Double percentage = (consumedCalories / Constants.MAXIMUM_CALORIES) * 100;

            arcProgress.setProgress(percentage.intValue());
            caloriesCount.setText(String.format(getString(R.string.calories_daily), consumedCalories));
        }catch (Exception e){
            e.printStackTrace();
        }
        swipeRefresh.setRefreshing(false);

    }

    @Override
    public void onCaloriesLoadError(Object error) {
        String message = (String) error;
        swipeRefresh.setRefreshing(false);
        Toaster.ToastShort(getActivity(), message);
    }
}
