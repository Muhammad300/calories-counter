package com.anroid.ejazz.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.anroid.ejazz.R;
import com.anroid.ejazz.adapter.SearchCaloriesAdapter;
import com.anroid.ejazz.listeners.CaloriesAdded;
import com.anroid.ejazz.model.CaloriesModel;
import com.anroid.ejazz.presenter.CaloriesPresenter;
import com.anroid.ejazz.utils.Toaster;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class SearchCalories extends CaloriesData implements TextWatcher, SwipeRefreshLayout.OnRefreshListener, CaloriesAdded {
    @BindView(R.id.caloriesRecycler)
    RecyclerView caloriesRecycler;

    @BindView(R.id.swipeRefresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.searchItems)
    EditText search;
    private CaloriesPresenter caloriesPresenter;
    Unbinder unbinder;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        caloriesPresenter = new CaloriesPresenter(getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_items, container, false);
        unbinder = ButterKnife.bind(this, view);
        search.addTextChangedListener(this);
        caloriesRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        caloriesRecycler.setHasFixedSize(true);
        swipeRefresh.setOnRefreshListener(this);
        return view;
    }

    @Override
    public void onSuccess(Object data) {
        if (data instanceof CaloriesModel) {
            CaloriesModel caloriesModel = (CaloriesModel) data;

            SearchCaloriesAdapter searchCaloriesAdapter = new SearchCaloriesAdapter(getActivity(), (ArrayList<CaloriesModel.Hit>) caloriesModel.getHits(), getActivity().getSupportFragmentManager(), this);
            searchCaloriesAdapter.notifyDataSetChanged();
            caloriesRecycler.setAdapter(searchCaloriesAdapter);
            swipeRefresh.setRefreshing(false);
        }
    }

    @Override
    public void onError(String error) {
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        caloriesPresenter.fetch(s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        caloriesPresenter.cancel();
    }

    @Override
    public void onRefresh() {
        String keyword = search.getText().toString();
        if (!TextUtils.isEmpty(keyword)) {
            caloriesPresenter.fetch(keyword);
        }
    }

    @Override
    public void onCaloriesAdded(Object response) {
        String message = (String) response;
        Toaster.ToastShort(getActivity(), message);
    }

    @Override
    public void onCaloriesError(Object error) {
        String message = (String) error;
        Toaster.ToastShort(getActivity(), message);
    }
}
