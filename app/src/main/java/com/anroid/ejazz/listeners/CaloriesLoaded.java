package com.anroid.ejazz.listeners;

/**
 * Created by Muhammad on 6/16/2017.
 */

public interface CaloriesLoaded<T> {
    void onCaloriesLoaded(T response);
    void onCaloriesLoadError(T error);

}
