package com.anroid.ejazz.listeners;

/**
 * Created by Muhammad on 6/16/2017.
 */

public interface CaloriesAdded<T> {
    void onCaloriesAdded(T response);
    void onCaloriesError(T error);

}
