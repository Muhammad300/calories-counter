package com.anroid.ejazz.listeners;

/**
 * Created by Muhammad on 5/5/2016.
 */
public interface ConnectionStatus<T> {
    void onResponse(T response);
    void onError(T error);
}
