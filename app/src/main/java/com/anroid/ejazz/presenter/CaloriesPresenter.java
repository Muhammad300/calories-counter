package com.anroid.ejazz.presenter;

import android.content.Context;

import com.android.volley.Request;
import com.anroid.ejazz.R;
import com.anroid.ejazz.fragment.SearchCalories;
import com.anroid.ejazz.fragment.ConnectionData;
import com.anroid.ejazz.listeners.ConnectionStatus;
import com.anroid.ejazz.model.CaloriesModel;
import com.anroid.ejazz.network.ConnectToApi;
import com.anroid.ejazz.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Map;

/**
 * Created by Muhammad on 11/3/2016.
 */

public class CaloriesPresenter extends ConnectionData implements ConnectionStatus<Object> {

    private ConnectToApi connectToApi;
    private SearchCalories searchCalories;
    private Context context;

    public CaloriesPresenter(Context context, SearchCalories searchCalories) {
        connectToApi = new ConnectToApi(context, getURL(""), Request.Method.GET, this);
        this.searchCalories = searchCalories;
        this.context = context;
    }


    @Override
    public void onResponse(Object response) {
        Gson gson = new GsonBuilder().create();
        CaloriesModel caloriesModel = gson.fromJson((String) response, CaloriesModel.class);
        searchCalories.onSuccess(caloriesModel);
    }

    @Override
    public void onError(Object Error) {
        searchCalories.onError(context.getString(R.string.error));

    }


    @Override
    public void fetch(String keyword) {
        connectToApi.Connect(getURL(keyword),Request.Method.GET);
    }

    @Override
    public String getURL(String keyword) {
        return Constants.URL+keyword +Constants.RESULTS_LIMIT_PARAM+Constants.RESULTS_LIMIT+Constants.FIELDS+ Constants.APP_ID_PARAM +Constants.APP_ID+ Constants.APP_KEY_PARAM+Constants.APP_KEY;
    }

    @Override
    public Map<String, String> getParams() {
        return null;
    }

    @Override
    public void cancel() {
        connectToApi.Cancel();
    }
}
