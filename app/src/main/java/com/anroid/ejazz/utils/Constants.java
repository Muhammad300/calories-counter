package com.anroid.ejazz.utils;

/**
 * Created by Muhammad on 11/3/2016.
 */

public class Constants {
    //-----------------Connection Data---------------------------
    public final static String GET = "GET";
    public final static String POST = "POST";

    //-----------------Intervals---------------------------
    public final static int ConnectionTimeOut = 1000 * 60;
    public final static int DelayTime = 500;

    //-----------------Keys---------------------------
    public final static String APP_KEY  = "d00f2b8d26072d6c2b528c08ac66a445";
    public final static String APP_ID = "af6c4153";
    public final static String CLEAN_ACTION = "com.android.ejazz";
    public final static String DATE_FORMAT = "hh:mm a";

    public final static String RESULTS_LIMIT = "0:20";
    public final static double MAXIMUM_CALORIES = 2000;
    public final static String Request = "request";
    public final static String ItemName = "itemName";
    public final static String Calories = "calories";
    public final static String Time = "time";


    //-----------------URLS---------------------------

    public static final String URL = "https://api.nutritionix.com/v1_1/search/";
    public static final String APP_ID_PARAM ="&appId=";
    public static final String APP_KEY_PARAM ="&appKey=";
    public static final String RESULTS_LIMIT_PARAM ="?results=";
    public static final String FIELDS = "&fields=item_name,brand_name,item_id,nf_calories";






}
