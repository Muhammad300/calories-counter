package com.anroid.ejazz.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class Converter {


    public static String getCurrentTime(long millis) {
        return  (new SimpleDateFormat(Constants.DATE_FORMAT, Locale.US)).format(new Date(millis));
    }
}
