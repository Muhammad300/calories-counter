package com.anroid.ejazz.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Muhammad on 9/1/2016.
 */
public class Toaster {


    public static void ToastLong(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }


    public static void ToastShort(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
}
