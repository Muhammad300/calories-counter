package com.anroid.ejazz.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anroid.ejazz.R;
import com.anroid.ejazz.model.ConsumedCaloriesModel;
import com.anroid.ejazz.utils.Converter;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class ConsumedCaloriesAdapter extends RecyclerView.Adapter<ConsumedCaloriesAdapter.CaloriesHolder> {

    private CaloriesHolder caloriesHolder;
    private Context context;

    ArrayList<ConsumedCaloriesModel> consumedCaloriesModels;
    private FragmentManager fragmentManager;

    public ConsumedCaloriesAdapter(Context context, ArrayList<ConsumedCaloriesModel> consumedCaloriesModels, FragmentManager fragmentManager) {
        this.context = context;
        this.consumedCaloriesModels = consumedCaloriesModels;
        this.fragmentManager = fragmentManager;
    }

    @Override
    public CaloriesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item_today, parent, false);
        caloriesHolder = new CaloriesHolder(v);
        return caloriesHolder;
    }

    @Override
    public void onBindViewHolder(final CaloriesHolder holder, final int position) {
        try {
            holder.itemName.setText(consumedCaloriesModels.get(position).getItemName());
            holder.itemCalories.setText(String.format(context.getString(R.string.calories), consumedCaloriesModels.get(position).getCalories()));
            if (!consumedCaloriesModels.get(position).getTime().equalsIgnoreCase("")) {
                holder.time.setText(Converter.getCurrentTime(Long.valueOf(consumedCaloriesModels.get(position).getTime())));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    public void remove( int position) {
        consumedCaloriesModels.remove( consumedCaloriesModels.get(position));
        notifyItemRemoved(position);
    }
    public void add( int position) {
        consumedCaloriesModels.add(position, consumedCaloriesModels.get(position));
        notifyItemInserted(position);
    }



    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return consumedCaloriesModels.size();
    }

    public static class CaloriesHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemName)
        TextView itemName;
        @BindView(R.id.itemCalories)
        TextView itemCalories;
        @BindView(R.id.time)
        TextView time;


        public CaloriesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
