package com.anroid.ejazz.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.anroid.ejazz.R;
import com.anroid.ejazz.fragment.SearchCalories;
import com.anroid.ejazz.model.CaloriesModel;
import com.anroid.ejazz.storage.Database;
import com.anroid.ejazz.utils.Constants;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Muhammad on 6/16/2017.
 */

public class SearchCaloriesAdapter extends RecyclerView.Adapter<SearchCaloriesAdapter.CaloriesHolder> {

    private CaloriesHolder caloriesHolder;
    private Context context;

    ArrayList<CaloriesModel.Hit> caloriesModels;
    private FragmentManager fragmentManager;
    private Database database;
    private SearchCalories searchCalories;
    private Bundle bundle;

    public SearchCaloriesAdapter(Context context, ArrayList<CaloriesModel.Hit> caloriesModels, FragmentManager fragmentManager, SearchCalories searchCalories) {
        this.context = context;
        this.caloriesModels = caloriesModels;
        this.fragmentManager = fragmentManager;
        this.searchCalories = searchCalories;
        bundle = new Bundle();
        database = new Database(context, searchCalories);
    }

    @Override
    public CaloriesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_search_calories, parent, false);
        caloriesHolder = new CaloriesHolder(v);
        return caloriesHolder;
    }

    @Override
    public void onBindViewHolder(final CaloriesHolder holder, final int position) {
        try {
            final CaloriesModel.Hit caloriesModel = caloriesModels.get(position);
            holder.itemName.setText(caloriesModel.getFields().getItemName());
            holder.itemCalories.setText(String.format(context.getString(R.string.calories), caloriesModel.getFields().getNf_calories()));
            holder.mainCard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bundle.putString(Constants.ItemName, caloriesModel.getFields().getItemName());
                    bundle.putDouble(Constants.Calories, caloriesModel.getFields().getNf_calories());
                    bundle.putString(Constants.Time, System.currentTimeMillis() + "");
                    database.insertCaloriesData(bundle);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();

        }
    }


    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return caloriesModels.size();
    }

    public static class CaloriesHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.itemName)
        TextView itemName;
        @BindView(R.id.itemCalories)
        TextView itemCalories;
        @BindView(R.id.mainCard)
        CardView mainCard;


        public CaloriesHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
