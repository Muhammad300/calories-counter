package com.anroid.ejazz.network;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.anroid.ejazz.R;
import com.anroid.ejazz.listeners.ConnectionStatus;
import com.anroid.ejazz.utils.Constants;

import java.util.Map;

/**
 * Created by Muhammad on 11/3/2016.
 */

public class ConnectToApi implements Response.Listener<String>, Response.ErrorListener {
    VolleyRequest volleyRequest;
    Context context;
    ConnectionStatus<Object> connectionStatus;

    public ConnectToApi(Context context, String URL, int method, ConnectionStatus<Object> connectionStatus) {
        this.context = context;
        volleyRequest = new VolleyRequest(method, URL, this, this);
        this.connectionStatus = connectionStatus;
    }

    public void Connect(String URL,int method ) {
        if (ConnectionDetector.isNetworkAvailable(context)) {
            volleyRequest = new VolleyRequest(method, URL, this, this);
            volleyRequest.setShouldCache(false);
            volleyRequest.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.ConnectionTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(context).addToRequestQueue(volleyRequest);
        } else {
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    public void Cancel() {
        VolleySingleton.getInstance(context).cancelPendingRequests(Constants.Request);
    }

    public void postConnect(Map<String, String> params) {
        if (ConnectionDetector.isNetworkAvailable(context)) {
            volleyRequest.setShouldCache(false);
            volleyRequest.setParams(params);
            volleyRequest.setRetryPolicy(new DefaultRetryPolicy(
                    Constants.ConnectionTimeOut,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            VolleySingleton.getInstance(context).addToRequestQueue(volleyRequest);
        } else {
            Toast.makeText(context, context.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onResponse(String response) {
        try {
            connectionStatus.onResponse(response);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        connectionStatus.onError(error.getMessage());
    }
}
