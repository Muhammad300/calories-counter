package com.anroid.ejazz.network;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by Muhammad on 4/26/2016.
 */
public class VolleyRequest extends StringRequest {
    Map<String, String> params;
    String url;

    public VolleyRequest(int method, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {

        super(method, url, listener, errorListener);

    }


    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {
        return params;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> params = new HashMap<String, String>();
        params.put("Content-Type", "application/x-www-form-urlencoded");
        params.put("Authorization", "");
        return params;
    }
}
